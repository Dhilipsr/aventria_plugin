<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use App\OgeClone;
use TCG\Voyager\Events\BreadDataAdded;

class VoyagerOgeTemplateController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
     public function store(Request $request)
        {

            $slug = $this->getSlug($request);
            // return $slug ;
            $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

            // Check permission
            $this->authorize('add', app($dataType->model_name));

            // Validate fields with ajax
            $val = $this->validateBread($request->all(), $dataType->addRows);

            if ($val->fails()) {
                return response()->json(['errors' => $val->messages()]);
            }

            if (!$request->has('_validate')) {

            	
            	if($request->parent_id != null)
            	{
            		$data	= new $dataType->model_name;
            		$parent_data = OgeClone::findOrFail($request->parent_id);
            		

            		$data->title  = $request->title;
            		$data->slug  = $request->slug;
            		$data->parent_id  = $request->parent_id;
            		$data->logo  = $parent_data->logo;
            		$data->heading  = $parent_data->heading;
            		$data->step1_title  = $parent_data->step1_title;
            		$data->step1_subtitle  = $parent_data->step1_subtitle;
            		$data->step2_title  = $parent_data->step2_title;
            		$data->step2_subtitle  = $parent_data->step2_subtitle;
            		$data->step3_title  = $parent_data->step3_title;
            		$data->steP3_subtitle  = $parent_data->steP3_subtitle;
            		$data->step4_title  = $parent_data->step4_title;
            		$data->step4_subtitle  = $parent_data->step4_subtitle;
            		$data->sponsor_logo  = $parent_data->sponsor_logo;
            		$data->sponsor_logo_display  = $parent_data->sponsor_logo_display;
            		$data->footer_logo  = $parent_data->footer_logo;
            		$data->help_link  = $parent_data->help_link;
            		$data->copyright_text  = $parent_data->copyright_text;
            		$data->code  = $parent_data->code;

            		$data->save();
            	}
            	else
            	{
                	$data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
            	}

                event(new BreadDataAdded($dataType, $data));

                if ($request->ajax()) {
                    return response()->json(['success' => true, 'data' => $data]);
                }

                return redirect()
                    ->route("voyager.{$dataType->slug}.index")
                    ->with([
                            'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                            'alert-type' => 'success',
                        ]);
            }
        }

    // public function update(Request $request,)
    // {
    //     # code...
    // }
}
