<?php

namespace App\Http\Controllers;

use TCG\Voyager\Http\Controllers\VoyagerController as BaseVoyagerController;
use App\UserLog;
use Auth;

class VoyagerController extends BaseVoyagerController
{
    public function logout()
    {
    	if(Auth::user()->role_id != 1)
    	{
    		$log = UserLog::whereUnqId(session('unq_id'))->update(['logout_at'=> date('Y-m-d H:i:s')]);

    		session()->forget('unq_id');
    	}
        Auth::logout();

        return redirect()->route('voyager.login');
    }
}
