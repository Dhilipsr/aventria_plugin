<?php

namespace App\Http\Controllers;

use TCG\Voyager\Http\Controllers\VoyagerAuthController as BaseVoyagerAuthController;
use Illuminate\Http\Request;
use App\UserLog;
use App\User;
use Date;

class VoyagerAuthController extends BaseVoyagerAuthController
{
    public function postLogin(Request $request)
    {

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->credentials($request);

        if ($this->guard()->attempt($credentials, $request->has('remember'))) {
        	$user = User::whereEmail($request->email)->firstOrFail();
        	if($user->role_id != 1)
        	{
                $id = rand();
        		$data = array('unq_id' => $id,'user_name' => $user->name,'login_at' => date('Y-m-d H:i:s') );
        		$log = UserLog::create($data);
                session(['unq_id' => $id]);

        	}
              return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
}
