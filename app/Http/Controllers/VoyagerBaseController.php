<?php

namespace App\Http\Controllers;

use TCG\Voyager\Http\Controllers\VoyagerBaseController as BaseVoyagerBaseController;
use Illuminate\Http\Request;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadDataAdded;
use App\UserLog;
use TCG\Voyager\Facades\Voyager;
use Auth;
use App\OgeClone;

class VoyagerBaseController extends BaseVoyagerBaseController
{
	public function store(Request $request)
        {

            $slug = $this->getSlug($request);
            // return $slug ;
            $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

            // Check permission
            $this->authorize('add', app($dataType->model_name));

            // Validate fields with ajax
            $val = $this->validateBread($request->all(), $dataType->addRows);

            if ($val->fails()) {
                return response()->json(['errors' => $val->messages()]);
            }

            if (!$request->has('_validate')) {

            	if($slug == 'oge-template')
            	{
	            	if($request->parent_id != null)
	            	{
	            		$data	= new $dataType->model_name;
	            		$parent_data = OgeClone::findOrFail($request->parent_id);
	            		

	            		$data->title  = $request->title;
	            		$data->slug  = $request->slug;
	            		$data->parent_id  = $request->parent_id;
	            		$data->logo  = $parent_data->logo;
	            		$data->heading  = $parent_data->heading;
	            		$data->step1_title  = $parent_data->step1_title;
	            		$data->step1_subtitle  = $parent_data->step1_subtitle;
	            		$data->step2_title  = $parent_data->step2_title;
	            		$data->step2_subtitle  = $parent_data->step2_subtitle;
	            		$data->step3_title  = $parent_data->step3_title;
	            		$data->steP3_subtitle  = $parent_data->steP3_subtitle;
	            		$data->step4_title  = $parent_data->step4_title;
	            		$data->step4_subtitle  = $parent_data->step4_subtitle;
	            		$data->sponsor_logo  = $parent_data->sponsor_logo;
	            		$data->sponsor_logo_display  = $parent_data->sponsor_logo_display;
	            		$data->footer_logo  = $parent_data->footer_logo;
	            		$data->help_link  = $parent_data->help_link;
	            		$data->copyright_text  = $parent_data->copyright_text;
	            		$data->code  = $parent_data->code;

	            		$data->save();
	            	}
	            	else
	            	{
	                	$data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
	            	}

            	}
            	else
            	{
            		$data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
            	}

                event(new BreadDataAdded($dataType, $data));

                if ($request->ajax()) {
                    return response()->json(['success' => true, 'data' => $data]);
                }

                return redirect()
                    ->route("voyager.{$dataType->slug}.index")
                    ->with([
                            'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                            'alert-type' => 'success',
                        ]);
            }
        }

    public function update(Request $request, $id)
    {

        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
    	

        // Compatibility with Model binding.
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

        event(new BreadDataUpdated($dataType, $data));

        if(Auth::user()->role_id != 1)
        {
        	$log = UserLog::whereUnqId(session('unq_id'))->first();
                $model_data = $dataType->model_name::find($id);
        
                if($log->model == '')
                {
                	$model = array();
                	array_push($model,$slug);
                	$new_data = json_encode($model);
                }
                else
                {
                	$model = json_decode($log->model);
                	array_push($model,$slug);
                	$new_data = json_encode($model);
                }
        
                if($log->title == '')
                {
                	$title = array();
          
        	        switch ($slug) 
        	        {
        	        	case 'clients':
        	        		array_push($title,$model_data->name);
        	        		break;
        
        
        	        	case 'oge-template':
        	        		array_push($title,$model_data->title);
        	        		break;
        
        
        	        	case 'resources':
        	        		array_push($title,$model_data->title);
        	        		break;
        	        	
        	        	default:
        	        		array_push($title,$model_data->title);
        	        		break;
        	        }
                }
                else
                {
                	$title = json_decode($log->title);
                	
                	switch ($slug) 
        	        {
        	        	case 'clients':
        	        		array_push($title,$model_data->name);
        	        		break;
        
        
        	        	case 'oge-template':
        	        		array_push($title,$model_data->title);
        	        		break;
        
        
        	        	case 'resources':
        	        		array_push($title,$model_data->title);
        	        		break;
        	        	
        	        	default:
        	        		array_push($title,$model_data->title);
        	        		break;
        	        }
                }
        		
                $check = $log->update(['model' => $new_data,'title' => json_encode($title)]);
        }
       

        if (auth()->user()->can('browse', $model)) {
            $redirect = redirect()->route("voyager.{$dataType->slug}.index");
        } else {
            $redirect = redirect()->back();
        }

        return $redirect->with([
            'message'    => __('voyager::generic.successfully_updated')." {$dataType->getTranslatedAttribute('display_name_singular')}",
            'alert-type' => 'success',
        ]);
    }
}
