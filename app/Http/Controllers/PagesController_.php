<?php

namespace App\Http\Controllers;

use Mail;
use App\Link;
use App\Client;
use App\Footer;
use App\Header;
use App\OgeClone;
use App\Resource;
use App\Hamburger;
use App\ClientTool;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
        $clone = OgeClone::where('is_default',1)->first();
        $resources = Resource::wherePlenvuCloneId($clone->id)->get();
       $client = '';
        // return $resources ;
        return view('welcome',compact(['resources','clone','client']));
    }

    public function tools($slug)
    {
        $clone = OgeClone::where('slug',$slug)->first();
        if(!$clone)
        {
            return abort(404);
        }
        $client_page = false;
        $resources = Resource::wherePlenvuCloneId($clone->id)->get();
        // return $resources ;
        $pro_cms = 0;
        $pro_vid = 0;
        $pro_pdf = 0;
        $pat_cms = 0;
        $pat_vid = 0;
        $pat_pdf = 0;
       foreach ($resources as $provider_pdf)
       {
           if($provider_pdf->menu_type == 1 and $provider_pdf->category_type == 1)
           {
                $pro_cms++;
           }
           elseif($provider_pdf->menu_type == 1 and $provider_pdf->category_type == 2)
           {
                $pro_vid++;
           }
           elseif($provider_pdf->menu_type == 1 and $provider_pdf->category_type == 3)
           {
                $pro_pdf++;
           }
           elseif($provider_pdf->menu_type == 2 and $provider_pdf->category_type == 1)
           {
                $pat_cms++;
           }
           elseif($provider_pdf->menu_type == 2 and $provider_pdf->category_type == 2)
           {
                $pat_vid++;
           }

           elseif($provider_pdf->menu_type == 2 and $provider_pdf->category_type == 3)
           {
                $pat_pdf++;
           }
       }
        $client = '';

        return view('welcome',compact(['resources','clone','client_page','pro_cms', 'pro_vid', 'pro_pdf', 'pat_cms', 'pat_vid',  'pat_pdf','client']));
    }

    public function orgIndex($clientSlug)
    {
        $resources = Resource::get();
        $client = Client::where('slug',$clientSlug)->first();
        $links = Link::first();
        // return $client;
    	return view('welcome',compact(['resources','client','links']));
    }

    // public function clientTools($slug,$clientSlug)
    public function clientTools(Request $request, $clientSlug)
    {
       $client = Client::where('slug',$clientSlug)->first();

       if (empty($client)) {
         return abort(404);
       }

       $clientHamburgers = $client->getHamburgers;
    //    return $clientHamburgers;
       $hamburgerResources = $client->getResources;
       $clientInfo = Client::where('slug',$clientSlug)->first();
       if(empty($clientInfo)) {
           return abort(404);
       }
       $firstHamburgerItem = $clientHamburgers->first();
       if($request->menu){
            // check if the


            if( ($clientHamburgers->pluck('slug'))->contains($request->menu) ){
                $resources = $clientHamburgers->where('slug',$request->menu)->first()->getResources;

            }else{
                return response()->json(['error' => 'Unauthenticated.'], 401);
            }

       }else{
            $resources = $clientHamburgers->first()->getResources;
       }

        $pro_cms = 0;
        $pro_vid = 0;
        $pro_fill_forms = 0;

        $pat_cms = 0;
        $pat_vid = 0;
        $pat_pdf = 0;
           foreach ($resources as $provider_pdf)
           {
               if($provider_pdf->menu_type == 1 and $provider_pdf->category_type == 1)
               {
                    $pro_cms++;
               }
               elseif($provider_pdf->menu_type == 1 and $provider_pdf->category_type == 2)
               {
                    $pro_vid++;
               }
               elseif($provider_pdf->menu_type == 1 and $provider_pdf->category_type == 3)
               {
                    $pro_fill_forms++;
               }

               elseif($provider_pdf->menu_type == 2 and $provider_pdf->category_type == 1)
               {
                    $pat_cms++;
               }
               elseif($provider_pdf->menu_type == 2 and $provider_pdf->category_type == 2)
               {
                    $pat_vid++;
               }

               elseif($provider_pdf->menu_type == 2 and $provider_pdf->category_type == 3)
               {
                    $pat_pdf++;
               }
           }

             return view('welcome',compact([
                 'clientHamburgers',
                 'resources',
                 'firstHamburgerItem',
                //  'client_page',
                 'clientInfo',
                 'pro_cms',
                  'pro_vid',
                   'pro_fill_forms',
                    'pat_cms',
                     'pat_vid',
                      'pat_pdf',
                      'client',
                    //   'client_tool'
            ]));
    //    }
    //    else
    //    {
    //        return abort(404);
    //    }
    }

    public function sendEmails(Request $request)
    {
        // return $request->all();
        $res_ids = explode(",",$request->res_ids);
        $emails = explode(',', $request->toEmail);
        $resources = Resource::whereIn('id',$res_ids)->get();
        $emailBody = '';

        foreach($resources as $res)
        {
            $url = '';
            if($res->separate_pi == "1")
            {
                $pi_pdf = json_decode($res->pdf_with_pi);
                $link = str_replace('\\','/',$pi_pdf[0]->download_link);
                $url = asset('storage/'.$link);
            }
            elseif($res->cms_link)
            {
                $url = $res->cms_link;
            }
            elseif($res->video_link)
            {
                $url = $res->video_link;
            }
            elseif($res->pdf != "[]")
            {
                // return dd($res->pdf);
                $pdf = json_decode($res->pdf);
                // return dd($pdf);
                if($pdf[0]->download_link)
                {
                    $link = str_replace('\\','/',$pdf[0]->download_link);
                    $url = asset('storage/'.$link);
                }

            }



            $emailBody .= '<b>Here is a resource that might be of interest. Please open the link to view:<br><br>'.$res->title.'</b><br>'.$res->english_description.'<br> <a href='.$url.'>'.$url.'</a> <br><br>';
        }


        // $emailBody = $request->emailBody;
        $data = array( 'emailBody'=> $emailBody );
        $checkMail = Mail::send('emails.sharedResource', $data, function($message) use ($emails)
        {
            $message->from('support@actonms.com');
            $message->to($emails)->subject('Multiple resources has been shared with you');
        });

        // if($checkMail){
            return redirect()->back()->with('emailSuccess','Email Sent Successfully.!');
        // }
    }

     public function viewPdf()
    {
        return response()->file(public_path('Novartis 8.5 X 11 PI.pdf'));
    }
}