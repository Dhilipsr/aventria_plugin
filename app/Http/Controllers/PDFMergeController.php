<?php

namespace App\Http\Controllers;

use App\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use LynX39\LaraPdfMerger\Facades\PdfMerger;

class PDFMergeController extends Controller
{
    public function pdfTools(Request $request)
    {	
        // return $request->all();
        
    	$pdfArray = [];
        // return $request->all();
    	$providers = $request->providerResources;
    	$providerResources = Resource::when($providers, function ($query, $providers) {
                    return $query->whereIn('id',$providers)->get();
                });
//        foreach ($providerResources as $res) {

//         $opts = [
//        "http" => [
//            "method" => "GET",
//            "header" =>
             
//                 "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n"
//                . "Accept-Language: en-US,en;q=0.5\r\n"
              
//        ],
//    ];
//    $url = $res->cms_link;

//    $context = stream_context_create($opts);
//    $data = file_get_contents($url, false, $context);
//    $filename = rand().".blade.php";

//    \Storage::disk('public')->put('temp_folder/'.$filename, $data);
//      $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
//         $ch = curl_init();
//          curl_setopt_array($ch, array(
//                 CURLOPT_URL => $res->cms_link,
//                 CURLOPT_RETURNTRANSFER => true,
//                 CURLOPT_ENCODING => "",
//                 CURLOPT_TIMEOUT => 30000,
//                 CURLOPT_FOLLOWLOCATION =>  true,
//                 CURLOPT_MAXREDIRS => 1,
//                 CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//                 CURLOPT_CUSTOMREQUEST => "GET",
//                 CURLOPT_HTTPHEADER => array(
//                     // Set Here Your Requesred Headers
//                     'Content-Type: text/html',
//                     'client-id:1a5ae8e4-5ab2-47cd-aca8-0a3f739d8a44',
//                     'client-secret:3bcf204d-8f2e-4768-9d1d-d4fdb1f7c44b'
//                 ),
//             ));
//              $data = curl_exec($ch);
//              return $data;
//    $pdf = \PDF::loadHtml($data);
//    return $pdf->download('post.pdf');

// return "okss";
//         $pdfArray[] = $filename; 
//        }

//         $file =  file_get_contents($providerResources[0]->cms_link);
//         return $file;
   
// return file_put_contents($tempImage, file_get_contents($providerResources[0]->cms_link));

// return response()->download($tempImage, $filename);


    	$providerFillableForm = $request->providerFillableForms;
    	$providerFillableForms = Resource::when($providerFillableForm, function ($query, $providerFillableForm) {
                    return $query->whereIn('id',$providerFillableForm)->get();
                });

    	$CPResource = $request->CPResources;
    	$CPResources = Resource::when($CPResource, function ($query, $CPResource) {
                    return $query->whereIn('id',$CPResource)->get();
                });

    	$CPSpanishResource = $request->CPSpanishResources;
    	$CPSpanishResources = Resource::when($CPSpanishResource, function ($query, $CPSpanishResource) {
                    return $query->whereIn('id',$CPSpanishResource)->get();
                });
    	
    	$VideoResource = $request->VideoResources;
    	$VideoResources = Resource::when($VideoResource, function ($query, $VideoResource) {
                    return $query->whereIn('id',$VideoResource)->get();
                });

    	foreach ($providerResources as $providerResource) {
    		if ($providerResource->can_print) {
    			$file = (json_decode($providerResource->pdf))[0]->download_link;
    			$pdfArray[] = $file;
    		}
    	}

    	foreach ($providerFillableForms as $providerFillableForm) {
    		if ($providerFillableForm->can_print) {
    			$file = (json_decode($providerFillableForm->pdf))[0]->download_link;
    			$pdfArray[] = $file;
    		}
    	}

    	foreach ($CPResources as $CPResource) {
    		if ($CPResource->can_print) {
    			$file = (json_decode($CPResource->pdf))[0]->download_link;
    			$pdfArray[] = $file;
    		}
    	}

    	foreach ($CPSpanishResources as $CPSpanishResource) {
    		if ($CPSpanishResource->can_print) {
    			$file = (json_decode($CPSpanishResource->pdf))[0]->download_link;
    			$pdfArray[] = $file;
    		}
    	}

    	foreach ($VideoResources as $VideoResource) {
    		if ($VideoResource->can_print) {
    			$file = (json_decode($VideoResource->pdf))[0]->download_link;
    			$pdfArray[] = $file;
    		}
    	}
        // return storage_path();
    	$pdfMerger = PDFMerger::init(); //Initialize the merger
    	foreach ( $pdfArray as $fileToMerge) {
            $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
            $fileToMerge = str_replace('\\', '/', $fileToMerge);
            $fileToMerge = $storagePath.'public/'.$fileToMerge;
            $pdfMerger->addPDF($fileToMerge, 'all');
    		// $pdfMerger->addPDF('storage/'.$fileToMerge, 'all');
            // $pdfMerger->addPDF(public_path('storage/').$fileToMerge, 'all');
    	}
    	// if($request->providerResources)
    	// {  
    	  
    	   
    	//      if( in_array('1', $request->providerResources) == true )
    	//      {
    	       
    	//          $pdfMerger->addPDF('pdf/Pi_pages.pdf','all');
    	//      }
    	//      else if(in_array('2',  $request->providerResources) == true)
     //         {
     //             $pdfMerger->addPDF('pdf/Pi_pages.pdf','all');
     //         }
    	     
    	// }
    	// elseif($request->CPResources)
    	// {
    	//     if(in_array('5', $request->CPResources) == true)
    	//     {
    	//          $pdfMerger->addPDF('pdf/Pi_pages.pdf','all');
    	//     }
    	// }
    	// elseif($request->CPSpanishResources)
    	// {
    	//     if(in_array('5', $request->CPSpanishResources) == true)
    	//     {
    	//          $pdfMerger->addPDF('pdf/Pi_pages.pdf','all');
    	//     }
    	// }
     //    elseif($request->providerFillableForms)
     //    {
     //        if(in_array('3', $request->providerFillableForms) == true)
     //        {
     //             $pdfMerger->addPDF('pdf/Pi_pages.pdf','all');
     //        }
     //        else if( in_array('4', $request->providerFillableForms) == true )
     //         {
               
     //             $pdfMerger->addPDF('pdf/Pi_pages.pdf','all');
     //         }
     //    }
    	
  
       
    	$pdfMerger->merge();
    	
    	$pdfMerger->save("file_name.pdf", "browser");
    }
}
