<?php

namespace App\Http\Controllers;

use App\Client;
use App\Resource;
use Illuminate\Http\Request;
use Mail;

class PagesController extends Controller
{
    
     public function duplicateResource(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', 'resources')->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        // $this->eagerLoadRelations($dataTypeContent, $dataType, 'edit', $isModelTranslatable);

        $view = 'vendor.voyager.resources.duplicate';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }



    public function index()
    {	
    	$resources = Resource::where('status','=',1)->get();
    	$client = '';
    	$client_id = '';
    	foreach($resources as $resource)
    	{
    	    $client_id = $resource->client_id;
    	}
    	if(!empty($client_id))
    	{
    	    $client = Client::where('status','=',1)->where('id',$client_id)->firstOrFail();
    	}
    	return view('welcome')->with(compact([
    		'resources','client'
    	]));
    }

        public function indexnew()
        {
            return abort(404);
        }

        
    public function orgIndex($clientSlug)
    {
        $client = Client::where('slug',$clientSlug)->firstOrFail();
        //return $client['id'];
        $resources = Resource::where('status','=',1)->where('client_id','=',$client['id'])->get();
    	return view('welcome',compact(['resources','client']));
    }

    public function sendEmails(Request $request)
    {
        // return $request->all();
    //   $request->all();
      
    //   print_r($request->all());
    //   exit;
   
      
        if($request->isMethod('post'))
        {
            $res_ids = array_map('intval', explode(',', $request->res_ids));
            if($this->arrayHasOnlyInts($res_ids))
            {
            $emails = explode(',', $request->toEmail);
            $resources = Resource::whereIn('id',$res_ids)->get();
            $emailBody = '';
            
            foreach($resources as $res)
            {
                
            //      print($res);
            // exit;
                $url = '';
               
                if($res->separate_pi == "1")
                
                {
                  
                     echo $url;
                    exit;
                    $pi_pdf = json_decode($res->pdf_with_pi);
                    $url = asset('storage/'.$pi_pdf[0]->download_link);
                }
                elseif($res->cms_link)
                {
                    // for($i=0; $i<$res->cms_link; $i++)
                    // {
                     $url = $res->cms_link;
                   // echo count(array($url));
                    // exit;
                    // }
                }
                elseif($res->video_link)
                {
                    $url = $res->video_link;
                }
                elseif($res->pdf != "[]")
                {
                    
                    // return dd($res->pdf);
                    $pdf = json_decode($res->pdf);
                      
                    // ptint_r($pdf);
                    // exit;
                    // return dd($pdf);
                    if($pdf[0]->download_link)
                    {
                    $url = asset('storage/'.$pdf[0]->download_link);
                    }
                   
                }
                
                
    
                $emailBody .= '<b>Here is a resource that might be of interest. Please open the link to view:<br><br>'.$res->title.'</b><br>'.$res->english_description.'<br> <a href='.$url.'>'.$url.'</a> <br><br>';
            }
            
            
            // $emailBody = $request->emailBody;
            $data = array( 'emailBody'=> $emailBody );
            //   echo count(array($data));
            $count=0;
              if($data)
              {
                  $count++;
              }
            //   $x=0;
              
            //   while($x<=5)
            //   {
            //   if($count >1)
            //   {
            // $checkMail = Mail::send('emails.sharedResource', $data, function($message) use ($emails)
            // {    
                // if( $emailBody->cms_link >1)
                // {
                
                // $message->from('support@liverlifepro.com');
                // $message->to($emails)->subject('Multiple resources has been shared with you');    
                // }
                // else
                // {
                //       $message->from('support@liverlifepro.com');
                // $message->to($emails)->subject('single resources has been shared with you');    
                // }
                
            // });
            //   }
            //   }
              
              if($count>1)
              {
                  echo "success";
                  exit;
              }
              if($count==1)
              {
                  if(count($res_ids) <= 1)
                  {
                    $checkMail = Mail::send('emails.sharedResource', $data, function($message) use ($emails)
                    {    
                        // if( $emailBody->cms_link >1)
                        // {
                        
                        $message->from('support@liverlifepro.com');
                        $message->to($emails)->subject('A resource has been shared with you');    
                        // }
                        // else
                        // {
                        //       $message->from('support@liverlifepro.com');
                        // $message->to($emails)->subject('single resources has been shared with you');    
                        // }
                        
                    });
                  }
                  else
                  {
                      $checkMail = Mail::send('emails.sharedResource', $data, function($message) use ($emails)
                    {    
                        // if( $emailBody->cms_link >1)
                        // {
                        
                        $message->from('support@liverlifepro.com');
                        $message->to($emails)->subject('Multiple resources have been shared with you');    
                        // }
                        // else
                        // {
                        //       $message->from('support@liverlifepro.com');
                        // $message->to($emails)->subject('single resources has been shared with you');    
                        // }
                        
                    });
                  }
              }
    
            // if($checkMail){
                return redirect()->back()->with('emailSuccess','Email Sent Successfully.!');
            }
            else
            {
                 return redirect()->back()->with('emailError','Something Went Wrong.');
            }
        }
        else
            { return abort(404); }
    }
    
     public function viewPdf()
    {
        return response()->file(public_path('Xifaxan 8.5 X 11 PI.pdf'));
    }
    
    function arrayHasOnlyInts($array)
    {
        foreach ($array as $value)
        {
            if (!is_int($value)) // there are several ways to do this
            {
                 return false;
            }
            
        }
        return true;
    }
    
    
}
