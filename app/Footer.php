<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Footer extends Model
{
    public function clone()
    {
    	return $this->belongsTo('App\PlenvuClone');
    }
}
