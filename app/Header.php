<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Header extends Model
{
    public function clone()
    {
    	return $this->belongsTo('App\PlenvuClone');
    }
}
