<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Client extends Model
{   
    public function getHamburgers()
    {
        return $this->belongsToMany('App\Hamburger','client_hamburger','client_id','hamburger_id');
    }

}
 