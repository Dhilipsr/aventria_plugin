<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    protected $fillable = ['unq_id','user_name','login_at','logout_at','model','title'];
}
