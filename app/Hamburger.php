<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Hamburger extends Model
{
    public function getResources()
    {
        return $this->belongsToMany('App\Resource','hamburgers_resources','hamburger_id','resource_id');
    }
}