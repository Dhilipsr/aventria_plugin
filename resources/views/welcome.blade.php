@section('title')
XIFAXAN – QSA – PLUGIN
@endsection
@section('css')
@endsection
@extends('layouts.layout')
@section('content')

 <div class="frame">


</style> 
<!-- Main Section -->
<form class="form" method="POST" action="{{route('pdfMerge')}}" target="_blank|_self|_parent|_top|framename"> 
    @csrf



    <section class="side-space main-section">
        <section class="container">
            <div class="row" >
                
                <div class="col-12 tools">
                    <nav class="notification">
                        <ul>
                            <li class="color"><img style="margin-bottom: 2%;" src="{{asset('img/ds.png')}}"><span> Select tool(s)</span></li>
                            <li  class="color"><img style="width:24px;" src="{{asset('img/paint.png')}}"> Preview tool</li>
                            <li  class ="color2"><img style="margin-bottom: -1%;" src="{{asset('img/dow1.png')}}"> Description</li>
                           
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="provider_parent" style="">
                <div id="box" class="provider_child" style="">
                    <div class="row provider_padding" style="padding-left: 70px;"><h2 class="provider_h2" style=""><strong style="font-weight:700;">Provider</strong></h2></div>
                </div>
            </div>
<style type="text/css">
    #box
    {
        width: 1000px;
        height: 55px;
    }
</style>


            <div class="" style="background-color: #F5F5F5;">
                <div class="row " style="padding-top : 25px; " >
                    <div class="col-12 imp-safet"  style="padding-left: 70px;">
                        <div class="row " >
                            <div class="col-12">
                                <div class="pd-lef" >
                                    <div class="acc pd-lef" >
                                        @foreach( $resources as $resource )
                                            @if( $resource->menu_type == 2 && $resource->category_type == 1 )
                                                @php
                                                if($resource->separate_pi == 1)
                                                {

                                                    $pi_pdf = json_decode($resource->pdf_with_pi);
                                                }
                                                else
                                                {
                                                    $pdf = json_decode($resource->pdf);
                                                }
                                                @endphp
                                                <div class="acc__card ">
                                                    <div class="acc__title neq" style="background-color: #F5F5F5; font-weight: bolder; font-family: Open Sans;  color: #3D858D; ">

                                                        <style >.neq {
                                                          
                                                          font-size: 18px;
                                                        
                                                        }</style>

                                                            <style type="text/css">
                                                                
                                                            </style>
                                                        <span class="form-group"  >
                                                            <input type="checkbox"  name="CPResources[]" class="checkbox gacheckbox "  id="{{$resource->id}}" value="{{$resource->id}}"
                                                            data-english_description="{{$resource->english_description}}"
                                                            data-spanish_description="{{$resource->spanish_description}}"
                                                             data-title="{{$resource->title}}"


                                                            @if($resource->separate_pi == 1)
                                                          
                                                               data-tool-url = "{{$resource->cms_link}}"
                                                                data-print-url="{{asset('storage/'.$pi_pdf[0]->download_link)}}"
                                                                 data-my-cms-url = "{{$resource->cms_link}}"
                                                               
                                                                data-can-print="{{$resource->can_print}}"
                                                            @elseif(!empty($pdf[0]->download_link))
                                                                data-tool-url = "{{$resource->cms_link}}"
                                                                data-print-url="{{asset('storage/'.$pdf[0]->download_link)}}"
                                                                data-my-cms-url = "{{$resource->cms_link}}"
                                                               
                                                                data-can-print="{{$resource->can_print}}"
                                                            @elseif($resource->cms_link)
                                                                data-tool-url =  "{{$resource->cms_link}}"
                                                                data-print-url = "{{$resource->cms_link}}"
                                                                data-can-print="{{$resource->can_print}}"
                                                                 data-my-cms-url = "{{$resource->cms_link}}"
                                                            @endif
                                                            >
                                                            <label for="{{$resource->id}}">{{$resource->title}}
                                                            </label>
                                                            @if($resource->can_print == 0)
                                                                @if($resource->no_print_title == 1)
                                                                 
                                                                @endif
                                                            @endif
                                                        </span>
                                                        @if($resource->cms_link)
                                                            <a href="{{$resource->cms_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                                    <img style="width:29px;" src="{{asset('img/nw1111.png')}}">
                                                            </a>
                                                        @else
                                                        @if($resource->separate_pi ==1)
                                                            <a href="{{asset('storage/'.$pi_pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                                <img style="width:29px;" src="{{asset('img/nw1111.png')}}">
                                                            </a>
                                                         @elseif(!empty($pdf[0]->download_link))
                                                            <a href="{{asset('storage/'.$pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                                <img src="{{asset('img/nw12.png')}}">
                                                            </a>
                                                         @endif
                                                        @endif
                                                        {{-- <a class="preview"><img src="{{asset('img/nw111.png')}}"></a> --}}
                                                        <a class="drop-down"><img src="{{asset('img/dow.png')}}"></a>
                                                    </div>
                                                    <div class="acc__panel  mr-left mee" style="background-color: #F5F5F5; color: #3D858D">

                                                         <style >.mee {
                                                         /* font-weight: bold;*/
                                                         font-size: 16px;
                                                          font-weight: 600;
                                                          padding-left:0px;
                                                                }</style>
                                                                
                                                        {{$resource->english_description}}
                                                    
                                                       
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="patient_parent">
                <div id="box" class="patient_child">
                    <div class="row patient_padding" style="padding-left: 70px;"><h2 class="patient_h2"><strong style="font-weight:700;">Patient</strong></h2></div>
                </div>
            </div>
            <style type="text/css">
                #box
                {
                    width: 1000px;
                    height: 55px;
                   /* background-color: red;*/
                }
            </style>

           {{-- Provider pdf --}}
           <div class="" style="background-color: #F5F5F5;">
                <div class="row " style="padding-top : 25px; " >
                    <div class="col-12 imp-safet">
                        <div class="row ">
                            <div class="col-12 ">
                                <div class="pd-lef">
                                    <div class="acc pd-left">
                                        @foreach( $resources as $resource )
                                            @if( $resource->menu_type == 1 && $resource->category_type == 1 )
                                                @php
                                                   
                                                    if($resource->separate_pi == "1")
                                                    {
                                                        $pi_pdf = json_decode($resource->pdf_with_pi);
                                                    }
                                                    else
                                                    {
                                                         $pdf = json_decode($resource->pdf);
                                                    }
                                                @endphp
                                                <div class="acc__card" style="background-color: #F5F5F5;">

                                                    <div class="acc__title nn" style="background-color: #F5F5F5; font-family: Open Sans; color: #3D858D;" >


                                                         <style >.nn {
                                                            
                                                              font-size: 18px;
                                                              font-weight: bold;
                                                            }</style>



                                                                <style type="text/css">
                                                                    
                                                                </style>
                                                        <span class="form-group">
                                                            
                                                            <input type="checkbox"  name="providerResources[]" class="checkbox gacheckbox" id="{{$resource->id}}" value="{{$resource->id}}" 
                                                            data-title="{{$resource->title}}"
                                                            data-english_description="{{$resource->english_description}}"
                                                            data-spanish_description="{{$resource->spanish_description}}"
                                                                
                                                                @if($resource->separate_pi == 1)
                                                                    data-tool-url = "{{$resource->cms_link}}"
                                                                    data-print-url="{{asset('storage/'.$pi_pdf[0]->download_link)}}"
                                                                    data-title="{{$resource->title}}"
                                                                    data-can-print="{{$resource->can_print}}"
                                                                     data-my-cms-url = "{{$resource->cms_link}}"

                                                               @elseif(!empty($pdf[0]->download_link))
                                                                    data-tool-url= "{{$resource->cms_link}}"
                                                                    data-print-url="{{asset('storage/'.$pdf[0]->download_link)}}"
                                                                    data-title="{{$resource->title}}"
                                                                    data-can-print="{{$resource->can_print}}"
                                                                     data-my-cms-url = "{{$resource->cms_link}}"
                                                                @elseif($resource->cms_link)
                                                                    data-tool-url = "{{$resource->cms_link}}"
                                                                    data-print-url = "{{$resource->cms_link}}"
                                                                    data-can-print="{{$resource->can_print}}"
                                                                     data-my-cms-url = "{{$resource->cms_link}}"
                                                                @endif
                                                           
                                                            >
                                                            <label  for="{{$resource->id}}">{{$resource->title}}</label>
                                                        </span>
                                                        @if($resource->cms_link)
                                                            <a href="{{$resource->cms_link}}" target="_blank" class="preview">
                                                                    <img style="width:29px;" src="{{asset('img/nw1111.png')}}">
                                                            </a>
                                                        @else
                                                            @if($resource->separate_pi == 1)
                                                                 <a href="{{asset('storage/'.$pi_pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                                    <img style="width:29px;" src="{{asset('img/nw1111.png')}}">
                                                                </a>
                                                            @elseif(!empty($pdf[0]->download_link))
                                                                <a href="{{asset('storage/'.$pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                                    <img style="width:29px;" src="{{asset('img/nw1111.png')}}">
                                                                </a>
                                                            @endif
                                                        @endif
                                                        <a class="drop-down ">
                                                            <img src="{{asset('img/dow.png')}}">
                                                        </a>
                                                    </div>
                                                    <div class="acc__panel  mr-left me" style="background-color: #F5F5F5; color: #3D858D;">
                                                         <style >.me {
                                                              /*font-weight: bold;*/
                                                               font-weight: 600;
                                                               font-size: 16px;
                                                               padding-left:0px;
                                                            }</style>
                                                    
                                                       {{$resource->english_description}}
                                                       @if($resource->can_print == 0)
                                                        @if($resource->no_print_title == 1)
                                                           <!--<span class="note"> PLEASE NOTE: This tool can not be printed.</span>-->
                                                        @endif
                                                       @endif
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>


    <a class="back-to-top"  style="float: right; padding-right: 20px;">
        <div style="color: black; font-size: 20px; font-weight: 300px; font-family:'Open Sans'; padding-bottom: px;  color: black;" >
                 <img style="padding-bottom: 20px; " width="24px;" src="{{asset('img/pho.png')}}"> Back to Top
        </div>
    </a>
    <section class="containe bottom_buttons footer_buttons" style="">
        <div class="row" >

            <div class="col-12 d-flex justify-content-center blue_buttons">
                <a data-toggle="modal" style="color:#fff;" data-target="#myModal" class="button gaEmail buttonemail" id="email_res">Email</a>
                <button class="button viewPrint"  style="font-size: 20px;">View/Print</button>
                <button class="button gaCopy" id="copy-tool" >Copy Link</button>
            </div>
        </div>
    </section>
      
</form>

<!-- Main Section Over-->
<style>
.button {
  background-color: #007788; /* Green */
  border: none;
  color: white;
  font-size: 20px;

 
  text-decoration: none;
  display: inline-block;
  cursor: pointer;
}
.button {width:200px;}
.button {border-radius: 1px;}
.button {height: 45px;}
.button:hover {color: white;}
</style>



@endsection
@section('js')
<script type="text/javascript">
    function errorFunction(message) {
        event.preventDefault();
        $('#errorResponse').html(message);
        $('#errorResponse').fadeIn();
        setTimeout(function () { $('#errorResponse').fadeOut(); }, 3000);
    }
    var canPrintCheck = [];
    var cannotPrintCheck = [];
    var urls;
    var titles = [];
    var resource_ids = [];
    $(document).on('click', '.checkbox', function(event) {
        
        if (($(this).attr('data-can-print') == 1)) {
            if($(this).is(':checked')){
                canPrintCheck.push($(this).val());    
            }else{
                var index = canPrintCheck.indexOf($(this).val());
                if (index !== -1) canPrintCheck.splice(index, 1);
            }
            
        }else{
             if($(this).is(':checked')){
                cannotPrintCheck.push($(this).val());    
            }else{
                var index = cannotPrintCheck.indexOf($(this).val());
                if (index !== -1) cannotPrintCheck.splice(index, 1);
            }
        }
       
        urls = ($(':checkbox:checked').map(function() {
             return $(this).attr('data-print-url');
           }).get());
        
        titles = ($(':checkbox:checked').map(function() {
             return $(this).attr('data-title') + ' ';
           }).get());
           
            if($(this).is(':checked'))
            {   resource_ids.push($(this).attr('id'));
                console.log($(this).attr('data-title'));
                var emailBody = '<p id="eb-'+$(this).attr('id')+'" class = "ebclass"><b>Here is a resource that might be of interest. Please open the link to view:<br><br>'+$(this).attr('data-title')+'</b><br>'+$(this).attr('data-english_description')+'<br> <a href="'+$(this).attr('data-my-cms-url')+'">'+$(this).attr('data-my-cms-url')+'</a> <br><br>';
                $('#email-body').append(emailBody);
                
            }
            else
            {
                var removeItem = $(this).attr('id');

                resource_ids = jQuery.grep(resource_ids, function(value) {
                  return value != removeItem;
                });
                
                $('#eb-'+$(this).attr('id')).remove();
               
            }

        $('#email-body-hidden').val($('#email-body').html());
                
        var count = $('#email-body .ebclass').length ; 
        if(count == 1)
        {
            $('#ebres1').show();
            $('#ebresmul').hide(); 
        }
        else if(count >= 2)
        {
           $('#ebres1').hide();
            $('#ebresmul').show(); 
        }

    });
    
    $(document).on('click', '.viewPrint', function(event) {
        if(cannotPrintCheck.length === 0){
            if (canPrintCheck.length === 0) {
            errorFunction('Please select at least 1 printable tool')

            }else{
                urls = ($(':checkbox:checked').map(function() {
                    canPrintCheck.push(($(this).attr('data-can-print')));
                   }).get());
                if($.inArray("0", canPrintCheck) !== -1){
                    errorFunction('Please unselect the link which cannot be printed');
                }
            }
        }else{
            errorFunction('Please uncheck non-printable tool');
        }
           
    });
    
    var checkBoxCategory = @if(empty($client)) 'plugin' @else '{{$client->name}}' @endif ;
    
    $(function(){
        $('#email_res').on('click',function(){
           $('#res_ids').val(resource_ids);
        });
    });

</script>



@endsection

