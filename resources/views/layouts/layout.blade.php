<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset = "UTF-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title class="somethig">Liver Life</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="{{asset('img/lo.png')}}">
    <link rel="icon" href="{{asset('img/lo.png')}}" type="image/gif" sizes="16x16">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">

    {{-- Page level css --}}
    @yield('css')
    {{-- Page level css --}}
    
   
    
    <!-- Google Tag Manager -->
   <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-KMDGNBN');</script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <!-- Start Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KMDGNBN"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="frame ">
        @include('partials.header') 
        
        {{-- content starts here --}}
        @yield('content')
        {{-- content ends here --}}
        
        @include('partials.flash')
        
        @include('partials.footer')
    </div>
    {{-- Modal Starts here --}}
    @include('partials.modal')
    {{-- Modal Ends Here --}}

    <script src="{{asset('js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <script src="{{asset('js/vendor/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>
    
    {{-- Page level js --}}
    @yield('js')
    {{-- Page level js --}}
    <script src="{{asset('js/main.js')}}"></script>
</body>

</html>