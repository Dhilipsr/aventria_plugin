-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 17, 2021 at 05:56 AM
-- Server version: 5.7.34
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qsaccess_plugin`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `member_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_logo_status` tinyint(3) UNSIGNED DEFAULT NULL,
  `sponser_status` tinyint(3) UNSIGNED DEFAULT NULL,
  `hamburger_status` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`, `member_logo`, `member_logo_status`, `sponser_status`, `hamburger_status`) VALUES
(1, 'priyank', 'priyank', '1', '2020-05-27 16:45:00', '2020-07-09 16:24:07', NULL, 1, 1, 1),
(2, 'Test', 'test', '1', '2020-06-25 16:34:13', '2020-06-25 16:34:13', NULL, 0, 0, 0),
(5, 'HM Hospital', 'houstonmethodisthospital', '1', '2020-06-29 18:01:18', '2020-06-29 18:01:18', NULL, 0, 0, 0),
(6, 'Walgreens', 'walgreens', '1', '2020-07-06 15:08:54', '2020-07-06 15:08:54', NULL, 0, 0, 0),
(8, 'BAYLOR SWAS', 'baylorscottandwhiteallsaints', '1', '2020-07-06 15:13:28', '2020-07-06 15:13:28', NULL, 0, 0, 0),
(9, 'UHS', 'uhs', '1', '2020-07-06 15:13:51', '2020-07-06 15:13:51', NULL, 0, 0, 0),
(10, 'Integris', 'integris', '1', '2020-07-06 15:14:06', '2020-07-06 15:14:06', NULL, 0, 0, 0),
(11, 'BIDMC', 'bidmc', '1', '2020-07-06 15:14:24', '2020-07-06 15:14:24', NULL, 0, 0, 0),
(12, 'Baylor SWSFW', 'baylorscottandwhiteallsaintsfortworth', '1', '2020-07-09 16:26:24', '2020-07-09 16:26:24', NULL, 0, 0, 0),
(13, 'NYP-WCM', 'nypweillcornellmedicine', '1', '2020-07-09 16:27:16', '2020-07-09 16:27:16', NULL, 0, 0, 0),
(14, 'Forbes RegH', 'forbesregionalhospital', '1', '2020-07-15 03:49:47', '2020-07-15 03:49:47', NULL, 0, 0, 0),
(15, 'Tower Health', 'towerhealth', '1', '2020-07-17 19:07:12', '2020-07-17 19:07:12', NULL, 0, 0, 0),
(16, 'Ochsner H', 'ochsnerhealth', '1', '2020-07-17 19:08:13', '2020-07-17 19:08:13', NULL, 0, 0, 0),
(17, 'Uni of PMC', 'upmc2', '1', '2020-07-17 19:10:30', '2020-07-17 19:10:30', NULL, 0, 0, 0),
(18, 'Uni of PMC', 'upmc1', '1', '2020-07-17 19:11:48', '2020-07-17 19:11:48', NULL, 0, 0, 0),
(19, 'Uni HC', 'universityhospitalscleveland2', '1', '2020-07-17 19:12:00', '2020-07-17 19:13:31', NULL, 0, 0, 0),
(20, 'Uni HC', 'universityhospitalscleveland', '1', '2020-07-17 19:14:43', '2020-07-17 19:14:43', NULL, 0, 0, 0),
(21, 'Mercy HWH', 'mercyhealthwesthospital', '1', '2020-07-30 05:00:59', '2020-07-30 05:00:59', NULL, 0, 0, 0),
(22, 'einsteinMC', 'einsteinmedicalcenter', '1', '2020-08-26 20:23:49', '2020-08-26 20:23:49', NULL, 0, 0, 0),
(23, 'SJHMC', 'saintjosephshospitalandmedicalcenter', '1', '2020-08-27 02:09:42', '2020-08-27 02:09:42', NULL, 0, 0, 0),
(24, 'UPittMC', 'upmc3', '1', '2020-08-27 02:13:02', '2020-08-27 02:13:02', NULL, 0, 0, 0),
(25, 'BDMC', 'bannerdesertmedicalcenter', '1', '2020-08-27 02:14:00', '2020-08-27 02:14:00', NULL, 0, 0, 0),
(26, 'SFGH', 'sfgh', '1', '2020-08-27 14:42:18', '2020-08-27 14:42:18', NULL, 0, 0, 0),
(27, 'AdventHealth', 'adventhealth', '1', '2020-08-27 14:43:00', '2020-08-27 14:43:00', NULL, 0, 0, 0),
(28, 'BIDMC', 'bidmcspecialtypharmacy', '1', '2020-08-27 14:43:41', '2020-08-27 14:43:41', NULL, 0, 0, 0),
(29, 'GulfportM', 'gulfportmemorial', '1', '2020-08-27 14:44:51', '2020-08-27 14:44:51', NULL, 0, 0, 0),
(30, 'NYPWeill', 'nypweillcornellmedcolumbia', '1', '2020-08-27 14:46:57', '2020-08-27 14:46:57', NULL, 0, 0, 0),
(31, 'BIDMCP', 'bidmcpharmacy', '1', '2020-08-27 14:47:43', '2020-08-27 14:47:43', NULL, 0, 0, 0),
(32, 'Clark mh', 'clarkmemorialhealth', '1', '2020-08-27 14:48:37', '2020-08-27 14:48:37', NULL, 0, 0, 0),
(33, 'UMMC', 'universityofmsmedicalcenter', '1', '2020-08-27 14:49:22', '2020-08-27 14:49:22', NULL, 0, 0, 0),
(34, 'BNHTrih', 'bethesdanorthhospitaltrihealth', '1', '2020-08-27 14:49:57', '2020-08-27 14:49:57', NULL, 0, 0, 0),
(35, 'St. LukeH', 'stlukeshospital', '1', '2020-08-27 14:50:35', '2020-08-27 14:50:35', NULL, 0, 0, 0),
(36, 'SSMHSLU', 'ssmhealthsluhospital', '1', '2020-08-27 14:56:06', '2020-08-27 14:56:06', NULL, 0, 0, 0),
(37, 'AGHospital', 'alleghenygeneralhospital', '1', '2020-08-27 14:56:46', '2020-08-27 14:56:46', NULL, 0, 0, 0),
(38, 'UCSanFran', 'universityofcaliforniasanfrancisco', '1', '2020-08-27 14:57:19', '2020-08-27 14:57:19', NULL, 0, 0, 0),
(39, 'UniHospital', 'universityhospital', '1', '2020-08-27 14:57:47', '2020-08-27 14:57:47', NULL, 0, 0, 0),
(40, 'PreHospital', 'presbyterianhospital', '1', '2020-08-27 14:58:12', '2020-08-27 14:58:12', NULL, 0, 0, 0),
(41, 'UPHDMCLD', 'unitypointhealthdesmoinescenterforliverdisease', '1', '2020-08-27 14:58:55', '2020-08-27 14:58:55', NULL, 0, 0, 0),
(42, 'SElizabeth', 'stelizabeth', '1', '2020-08-27 14:59:23', '2020-08-27 14:59:23', NULL, 0, 0, 0),
(43, 'Sharp C', 'sharpcoronado', '1', '2020-08-27 14:59:50', '2020-08-27 14:59:50', NULL, 0, 0, 0),
(44, 'AbingtonJ', 'abingtonjefferson', '1', '2020-08-27 15:00:37', '2020-08-27 15:00:37', NULL, 0, 0, 0),
(45, 'GILGroup', 'gilivergroup', '1', '2020-08-27 15:01:06', '2020-08-27 15:01:06', NULL, 0, 0, 0),
(46, 'Virtua', 'virtua', '1', '2020-08-27 15:01:00', '2020-08-28 03:47:07', NULL, 0, 0, 0),
(49, 'UFH', 'universityoffloridahealth', '1', '2020-08-31 14:22:08', '2020-08-31 14:22:08', NULL, 0, 0, 0),
(50, 'DFB - TEST', 'debbuhsosky', '1', '2020-08-31 14:24:00', '2020-08-31 15:55:16', NULL, 0, 0, 0),
(52, 'LGH', 'lancastergeneralhospital', '1', '2020-09-08 17:59:37', '2020-09-08 17:59:37', NULL, 0, 0, 0),
(53, 'UFHShands', 'universityoffloridahealthshands', '1', '2020-09-10 15:24:49', '2020-09-10 15:24:49', NULL, 0, 0, 0),
(54, 'AHWaterman', 'adventhealthwaterman', '1', '2020-09-10 15:26:18', '2020-09-10 15:26:18', NULL, 0, 0, 0),
(55, 'BCLE', 'bluffcityliverexperts', '1', '2020-09-14 19:39:47', '2020-09-14 19:39:47', NULL, 0, 0, 0),
(56, 'GISIllinois', 'gisolutionsofillinois', '1', '2020-09-15 15:13:13', '2020-09-15 15:13:13', NULL, 0, 0, 0),
(57, 'HMA', 'hospitalmedicineassociates', '1', '2020-09-15 15:16:47', '2020-09-15 15:16:47', NULL, 0, 0, 0),
(58, 'baylorliverconsult1', 'baylorliverconsult1', '1', '2020-09-15 15:51:21', '2020-09-15 15:51:21', NULL, 0, 0, 0),
(59, 'baylorliverconsult2', 'baylorliverconsult2', '1', '2020-09-15 15:51:37', '2020-09-15 15:51:37', NULL, 0, 0, 0),
(60, 'thomasjeffersonuniv', 'thomasjeffersonuniv', '1', '2020-09-15 15:52:35', '2020-09-15 15:52:35', NULL, 0, 0, 0),
(61, 'crmhchealth', 'crmhchealth', '1', '2020-09-16 15:14:52', '2020-09-16 15:14:52', NULL, 0, 0, 0),
(62, 'vcuhealth1', 'vcuhealth1', '1', '2020-09-16 15:15:15', '2020-09-16 15:15:15', NULL, 0, 0, 0),
(63, 'vcuhealth2', 'vcuhealth2', '1', '2020-09-16 15:15:29', '2020-09-16 15:15:29', NULL, 0, 0, 0),
(64, 'atlantagastroathens', 'atlantagastroathens', '1', '2020-09-16 15:15:48', '2020-09-16 15:15:48', NULL, 0, 0, 0),
(65, 'illinoiscenterfordigestiveliverhealth', 'illinoiscenterfordigestiveliverhealth', '1', '2020-09-16 20:35:45', '2020-09-16 20:35:45', NULL, 0, 0, 0),
(66, 'athensgastroenterologyassociation', 'athensgastroenterologyassociation', '1', '2020-09-23 14:27:43', '2020-09-23 14:27:43', NULL, 0, 0, 0),
(67, 'goodsamaritanhospital', 'goodsamaritanhospital', '1', '2020-09-23 14:28:21', '2020-09-23 14:28:21', NULL, 0, 0, 0),
(68, 'UAMS', 'UAMS1', '1', '2020-09-25 20:52:46', '2020-09-25 20:52:46', NULL, 0, 0, 0),
(69, 'Gastroenterologist Specialist Jefferson Torresdale ', 'gastrospecjeffersontorresdale', '1', '2020-09-25 20:53:21', '2020-09-25 20:53:21', NULL, 0, 0, 0),
(70, 'Greenwood Gastroenterology Center', 'greenwoodgastrocenter', '1', '2020-09-25 20:53:41', '2020-09-25 20:53:41', NULL, 0, 0, 0),
(71, 'Walgreens Specialty', 'walgreensspecialty', '1', '2020-09-25 20:53:00', '2020-09-30 13:11:26', NULL, 0, 0, 0),
(72, 'Keystone Healthcare', 'keystonehealthcare', '1', '2020-09-28 20:04:36', '2020-09-28 20:04:36', NULL, 0, 0, 0),
(73, 'ASSOCIATES IN DIGESTIVE HEALTH', 'associatesindigestivehealth', '1', '2020-10-02 16:06:59', '2020-10-02 16:06:59', NULL, 0, 0, 0),
(74, 'Baylor Liver Consultants', 'baylorliverconsultants', '1', '2020-10-02 16:07:34', '2020-10-02 16:07:34', NULL, 0, 0, 0),
(75, 'The Liver Institute - Methodist Hospital', 'theliverinstitute_methodisthospital', '1', '2020-10-02 17:27:08', '2020-10-02 17:27:08', NULL, 0, 0, 0),
(76, 'methodisthospitalcommunitypharmacy', 'methodisthospitalcommunitypharmacy', '1', '2020-10-07 17:53:00', '2020-10-07 17:54:36', NULL, 0, 0, 0),
(77, 'digestivediseasesgroup', 'digestivediseasesgroup', '1', '2020-10-07 18:02:48', '2020-10-07 18:02:48', NULL, 0, 0, 0),
(78, 'digestivecarecenter', 'digestivecarecenter', '1', '2020-10-07 18:03:02', '2020-10-07 18:03:02', NULL, 0, 0, 0),
(79, 'murfreesboromedicalclinic', 'murfreesboromedicalclinic', '1', '2020-10-07 18:03:19', '2020-10-07 18:03:19', NULL, 0, 0, 0),
(80, 'upa', 'upa', '1', '2020-10-07 18:03:34', '2020-10-07 18:03:34', NULL, 0, 0, 0),
(81, 'trihealth', 'trihealth', '1', '2020-10-08 17:44:58', '2020-10-08 17:44:58', NULL, 0, 0, 0),
(82, 'mayoclinic', 'mayoclinic', '1', '2020-10-08 17:47:58', '2020-10-08 17:47:58', NULL, 0, 0, 0),
(83, 'texomalivercenter', 'texomalivercenter', '1', '2020-10-08 18:13:06', '2020-10-08 18:13:06', NULL, 0, 0, 0),
(84, 'digestivediseasecenter', 'digestivediseasecenter', '1', '2020-10-12 15:58:44', '2020-10-12 15:58:44', NULL, 0, 0, 0),
(85, 'Texoma Liver Center 2', 'texomalivercenter2', '1', '2020-10-12 16:13:00', '2020-10-14 16:11:53', NULL, 0, 0, 0),
(86, 'livercenteroftexas', 'livercenteroftexas', '1', '2020-10-13 17:41:41', '2020-10-13 17:41:41', NULL, 0, 0, 0),
(87, 'Baptist Health Lexington', 'baptisthealthlexington', '1', '2020-10-14 16:15:50', '2020-10-14 16:15:50', NULL, 0, 0, 0),
(88, 'Montefiore Medical Center Moses Division Hospital', 'montefioremedicalcentermoses', '1', '2020-10-15 16:01:43', '2020-10-15 16:01:43', NULL, 0, 0, 0),
(89, 'Promedica OutPatient Pharmacy', 'promedicaoutpatientpharmacy', '1', '2020-10-19 14:00:00', '2020-10-19 14:01:14', NULL, 0, 0, 0),
(90, 'Texas Health Care Digestive Care', 'texashealthcaredigestivecare', '1', '2020-10-19 14:02:00', '2020-10-19 14:02:25', NULL, 0, 0, 0),
(91, 'Unity Point Health', 'unitypointhealth', '1', '2020-10-22 13:55:05', '2020-10-22 13:55:05', NULL, 0, 0, 0),
(92, 'Baptist Health', 'baptisthealth', '1', '2020-10-22 13:55:29', '2020-10-22 13:55:29', NULL, 0, 0, 0),
(93, 'St. Elizabeth Healthcare', 'stelizabethhealthcare', '1', '2020-10-22 13:55:58', '2020-10-22 13:55:58', NULL, 0, 0, 0),
(94, 'University Health System Pavillion Pharmacy', 'universityhealthsystempavillionpharmacy', '1', '2020-10-22 13:56:23', '2020-10-22 13:56:23', NULL, 0, 0, 0),
(95, 'University of New Mexico', 'universityofnewmexico', '1', '2020-10-22 13:56:45', '2020-10-22 13:56:45', NULL, 0, 0, 0),
(97, 'University of Louisville', 'universitylouisville', '1', '2020-10-26 14:24:00', '2020-10-26 14:25:08', NULL, 0, 0, 0),
(98, 'Saint FrancisRx', 'saintfrancisrx', '1', '2020-10-26 14:25:00', '2020-10-26 14:25:45', NULL, 0, 0, 0),
(99, 'University Health System RBG Pharmacy', 'uhsrbgpharmacy', '1', '2020-10-26 15:54:00', '2020-10-26 15:56:25', NULL, 0, 0, 0),
(100, 'Summit Medical Group', 'summitmedicalgroup', '1', '2020-10-28 15:07:00', '2020-10-28 15:08:04', NULL, 0, 0, 0),
(101, 'Paramount medical specialty group', 'paramountmedicalspecialtygroup', '1', '2020-10-28 15:08:00', '2020-10-28 15:08:41', NULL, 0, 0, 0),
(102, 'Digestive Associates of Ohio', 'digestiveassociatesohio', '1', '2020-10-29 15:32:00', '2020-10-29 15:32:38', NULL, 0, 0, 0),
(103, 'Temple University Hospital', 'templeuniversityhospital', '1', '2020-10-29 15:32:00', '2020-10-29 15:33:09', NULL, 0, 0, 0),
(104, 'Rutgers', 'rutgers', '1', '2020-11-02 20:09:00', '2020-11-02 20:09:41', NULL, 0, 0, 0),
(105, 'Providence Gastro', 'providencegastro', '1', '2020-11-03 01:47:00', '2020-11-03 01:47:46', NULL, 0, 0, 0),
(106, 'Saint Joseph', 'saintjoseph', '1', '2020-11-03 01:48:00', '2020-11-03 01:48:18', NULL, 0, 0, 0),
(107, 'Gastroenterology Associaties of Chattanooga', 'gastroassocchattanooga', '1', '2020-11-06 19:43:00', '2020-11-09 14:43:40', NULL, 0, 0, 0),
(108, 'Ascension', 'ascension', '1', '2020-11-12 21:31:42', '2020-11-12 21:31:42', NULL, 0, 0, 0),
(109, 'Ogden GI Clinic', 'ogdengiclinic', '1', '2020-11-16 19:46:00', '2020-11-16 19:47:06', NULL, 0, 0, 0),
(110, 'Saint Francis Medical Center', 'saintfrancismedicalcenter', '1', '2020-11-20 20:17:00', '2020-11-20 20:17:36', NULL, 0, 0, 0),
(111, 'Walgreens Pharmacy Co.', 'walgreenspharmacy_ga', '1', '2020-11-20 20:17:00', '2020-11-23 22:20:17', NULL, 0, 0, 0),
(112, 'University of Cincinnati', 'universitycincinnati', '1', '2020-12-01 14:22:15', '2020-12-01 14:22:15', NULL, 0, 0, 0),
(113, 'SCRIPPS', 'scripps', '1', '2020-12-01 14:22:42', '2020-12-01 14:22:42', NULL, 0, 0, 0),
(114, 'Hunt Regional', 'huntregional', '1', '2020-12-03 17:19:00', '2020-12-03 17:20:04', NULL, 0, 0, 0),
(115, 'Walgreens Health System Tulsa', 'walgreenshealthtulsa', '1', '2020-12-03 17:20:00', '2020-12-03 17:20:30', NULL, 0, 0, 0),
(116, 'University of Washington', 'universityofwashington', '1', '2020-12-03 17:20:00', '2020-12-03 17:20:54', NULL, 0, 0, 0),
(117, 'Baylor Scott and White', 'baylorscottandwhitetemple', '1', '2020-12-08 19:42:09', '2020-12-08 19:42:09', NULL, 0, 0, 0),
(118, 'Memorial Hermann', 'memorialhermann', '1', '2020-12-08 19:44:32', '2020-12-08 19:44:32', NULL, 0, 0, 0),
(119, 'Tulane Medical Center', 'tulanemedicalcenter', '1', '2020-12-08 19:44:52', '2020-12-08 19:44:52', NULL, 0, 0, 0),
(120, 'Mehdi Khorsandi MD', 'mehdikhorsandimd', '1', '2020-12-08 19:45:10', '2020-12-08 19:45:10', NULL, 0, 0, 0),
(121, 'UC Davis Medical', 'ucdavismedical', '1', '2020-12-11 18:13:00', '2020-12-11 18:13:22', NULL, 0, 0, 0),
(122, 'UC Davis', 'ucdavis', '1', '2020-12-11 18:13:00', '2020-12-11 18:13:46', NULL, 0, 0, 0),
(123, 'MetroHealth', 'metrohealth', '1', '2020-12-15 16:18:00', '2020-12-15 16:19:05', NULL, 0, 0, 0),
(124, 'Baylor University Medical Center', 'bayloruniversitymedicalcenter', '1', '2020-12-15 16:19:00', '2020-12-15 16:19:34', NULL, 0, 0, 0),
(125, 'UPMC', 'upmc', '1', '2020-12-15 19:52:00', '2020-12-17 21:40:56', NULL, 0, 0, 0),
(126, 'Munson', 'munson', '1', '2020-12-17 21:40:00', '2020-12-17 21:40:31', NULL, 0, 0, 0),
(127, 'UCSD', 'ucsd', '1', '2020-12-18 14:53:25', '2020-12-18 14:53:25', NULL, 0, 0, 0),
(128, 'University of Chicago', 'universityofchicago', '1', '2020-12-18 14:53:42', '2020-12-18 14:53:42', NULL, 0, 0, 0),
(129, 'University Hepatologists', 'universityhepatologists', '1', '2020-12-21 14:55:00', '2020-12-21 14:56:02', NULL, 0, 0, 0),
(130, 'Davis Medical Center', 'davishealth', '1', '2020-12-21 14:56:00', '2020-12-21 14:56:29', NULL, 0, 0, 0),
(131, 'Mercy Health Fairfield', 'mercyhealthfairfield', '1', '2020-12-28 15:53:00', '2020-12-28 15:54:01', NULL, 0, 0, 0),
(132, 'Emory', 'emory', '1', '2021-01-05 15:09:00', '2021-01-05 15:09:45', NULL, 0, 0, 0),
(133, 'Northside', 'northside', '1', '2021-01-05 15:10:07', '2021-01-05 15:10:07', NULL, 0, 0, 0),
(134, 'Augusta University', 'augustauniv', '1', '2021-01-05 15:10:00', '2021-01-05 15:10:38', NULL, 0, 0, 0),
(135, 'Atlanta gastroenterology', 'atlantagastroenterology', '1', '2021-01-07 18:44:00', '2021-01-07 18:44:22', NULL, 0, 0, 0),
(136, 'atlanta gastroenterology associates', 'atlantagastroenterologyassociates', '1', '2021-01-07 18:44:00', '2021-01-07 18:45:07', NULL, 0, 0, 0),
(137, 'Memorial Hermann V2', 'memorialhermann2', '1', '2021-01-07 18:48:00', '2021-01-07 18:48:43', NULL, 0, 0, 0),
(138, 'Lewis Gale Physicians', 'lewisgalephysicians', '1', '2021-01-07 18:48:00', '2021-01-07 18:49:13', NULL, 0, 0, 0),
(139, 'Washington Gastroenterology', 'washingtongastro', '1', '2021-01-14 14:38:00', '2021-01-19 18:13:15', NULL, 0, 0, 0),
(140, 'Ascension Via Christi', 'acensionviachristi', '1', '2021-01-14 14:39:00', '2021-01-14 14:39:36', NULL, 0, 0, 0),
(141, 'Morrison Health', 'morrisonhealth', '1', '2021-01-19 18:16:00', '2021-01-19 18:16:33', NULL, 0, 0, 0),
(142, 'Liz Stewart/for function', 'lizstewartphc', '1', '2021-01-29 21:49:00', '2021-01-29 21:49:38', NULL, 0, 0, 0),
(143, 'Indiana University - DaLD clinic', 'iudaldclinic', '1', '2021-01-29 21:49:00', '2021-01-29 21:50:09', NULL, 0, 0, 0),
(144, 'Advanced Digestive Care', 'advanceddigestivecare', '1', '2021-02-05 18:07:00', '2021-02-05 18:07:16', NULL, 0, 0, 0),
(145, 'Bradley A. Connor, MD, PLLC', 'baconnormd', '1', '2021-02-05 18:07:00', '2021-02-05 18:07:41', NULL, 0, 0, 0),
(146, 'Pinehurst Medical Clinic', 'pinehurstmedicalclinic', '1', '2021-02-05 18:07:00', '2021-02-05 18:08:10', NULL, 0, 0, 0),
(147, 'Colon stomach and liver center', 'colonstomachlivercenter', '1', '2021-02-05 18:08:00', '2021-02-08 16:10:01', NULL, 0, 0, 0),
(148, 'University Health System', 'universityhealthsystem', '1', '2021-02-11 15:33:00', '2021-02-11 15:33:41', NULL, 0, 0, 0),
(149, 'Mhealth Fairview Hepatology', 'mhealthfairviewhepatology', '1', '2021-02-11 15:34:00', '2021-02-11 15:35:05', NULL, 0, 0, 0),
(150, 'Mhealth Fairview Hepatology Test', 'mhealthfairviewhepatologytest', '1', '2021-02-19 14:54:00', '2021-02-19 14:55:21', NULL, 0, 0, 0),
(151, 'Advanced Digestive Care P A: Gastro Florida', 'gastrofl', '1', '2021-02-24 16:02:00', '2021-02-24 16:02:49', NULL, 0, 0, 0),
(152, 'Iola Derm 101', 'ioladerm', '1', '2021-02-24 16:03:00', '2021-02-24 16:05:36', NULL, 0, 0, 0),
(153, 'Gastroenterology Associates', 'gastroenterologyassociatesva', '1', '2021-02-25 23:32:00', '2021-02-25 23:33:03', NULL, 0, 0, 0),
(154, 'Crozer Chester Medical Center', 'crozerchestermedicalcenter', '1', '2021-02-25 23:33:00', '2021-02-25 23:33:30', NULL, 0, 0, 0),
(155, 'Baylor Scott & White Dallas', 'baylorscottwhitedallas', '1', '2021-03-02 15:45:00', '2021-03-02 15:45:43', NULL, 0, 0, 0),
(156, 'Franciscan Health Indianapolis', 'franciscanhealthindianapolis', '1', '2021-03-02 15:45:00', '2021-03-02 15:46:08', NULL, 0, 0, 0),
(157, 'MMC user 1', 'mmc1', '1', '2021-03-04 16:53:00', '2021-03-04 16:53:28', NULL, 0, 0, 0),
(158, 'MMC User 2', 'mmc2', '1', '2021-03-04 16:53:00', '2021-03-04 16:53:52', NULL, 0, 0, 0),
(159, 'Vanderbilt', 'vanderbiltnashville', '1', '2021-03-04 16:54:00', '2021-03-04 16:54:21', NULL, 0, 0, 0),
(160, 'BGA user 1', 'bgapc1', '1', '2021-03-04 16:54:00', '2021-03-04 16:54:55', NULL, 0, 0, 0),
(161, 'BGA User 2', 'bgapc2', '1', '2021-03-04 16:55:00', '2021-03-04 16:55:28', NULL, 0, 0, 0),
(162, 'BGA user 3', 'bgapc3', '1', '2021-03-04 16:55:00', '2021-03-04 16:55:57', NULL, 0, 0, 0),
(163, 'Tri Star Gastroenterology Specialties of Middle TN', 'tristargastro', '1', '2021-03-04 16:56:00', '2021-03-04 16:56:30', NULL, 0, 0, 0),
(164, 'dr porcelli', 'drmporcelli', '1', '2021-03-04 16:56:00', '2021-03-04 16:56:57', NULL, 0, 0, 0),
(165, 'Baystate Health', 'baystatehealth', '1', '2021-03-04 16:57:00', '2021-03-04 16:57:26', NULL, 0, 0, 0),
(166, 'Tufts Medical Center', 'tuftsmedicalcenter', '1', '2021-03-04 16:57:00', '2021-03-04 16:57:58', NULL, 0, 0, 0),
(167, 'Hartford Hospital', 'hartfordhospital', '1', '2021-03-04 16:58:00', '2021-03-04 16:58:22', NULL, 0, 0, 0),
(168, 'Baylor Scott & White Medical Center - Temple', 'baylorscottwhitemedcentemple', '1', '2021-03-05 17:00:00', '2021-03-05 17:00:57', NULL, 0, 0, 0),
(169, 'UT Southwestern', 'utsouthwestern', '1', '2021-03-09 19:21:44', '2021-03-09 19:21:44', NULL, 0, 0, 0),
(170, 'UTSW-Clements University Hospital', 'utsw-clementsuniversityhospital', '1', '2021-03-09 19:34:00', '2021-03-09 19:34:36', NULL, 0, 0, 0),
(171, 'Illinois Gastro Group', 'illinoisgastrogroup', '1', '2021-03-09 19:35:00', '2021-03-09 19:35:35', NULL, 0, 0, 0),
(172, 'UMass Memorial Health Care', 'umassmemorialhealthcare', '1', '2021-03-16 16:20:00', '2021-03-16 16:20:16', NULL, 0, 0, 0),
(173, 'IBMC', 'ibmc', '1', '2021-03-18 13:50:00', '2021-03-18 13:50:51', NULL, 0, 0, 0),
(174, 'Combs-Jennie Stuart Gastroenterology', 'combs-jenniestuartgastro', '1', '2021-03-18 13:51:00', '2021-03-18 14:00:45', NULL, 0, 0, 0),
(175, 'Dayton-Jennie Stuart Gastroenterology', 'dayton-jenniestuartgastro', '1', '2021-03-18 13:51:00', '2021-03-18 13:56:30', NULL, 0, 0, 0),
(176, 'Bridgeforth-Jennie Stuart Gastroenteroolgy', 'bridge-jenniestuartgastro', '1', '2021-03-18 13:52:00', '2021-03-18 14:00:54', NULL, 0, 0, 0),
(177, 'Reading Hospital Kim', 'readinghospital2', '1', '2021-03-19 21:23:00', '2021-03-19 21:23:57', NULL, 0, 0, 0),
(178, 'Reading Hospital Chris', 'readinghospital1', '1', '2021-03-19 21:24:00', '2021-03-19 21:24:27', NULL, 0, 0, 0),
(179, 'OCHSNER Clinic Foundation', 'ochsnerclinicfoundation', '1', '2021-03-23 21:12:00', '2021-03-23 21:12:13', NULL, 0, 0, 0),
(180, 'OCHSNER Pharmacy and Wellness', 'ochsnerpharmacywellness', '1', '2021-03-23 21:12:00', '2021-03-23 21:12:42', NULL, 0, 0, 0),
(181, 'OCHSNER', 'ochsnerbatonrouge', '1', '2021-03-23 21:12:00', '2021-03-23 21:13:13', NULL, 0, 0, 0),
(182, 'Baptist Health Floyd', 'baptisthealthfloyd', '1', '2021-03-24 18:32:00', '2021-03-24 18:32:27', NULL, 0, 0, 0),
(183, 'Montefiore Medical Center', 'montefioremedicalcenter', '1', '2021-03-30 14:54:00', '2021-03-30 14:54:45', NULL, 0, 0, 0),
(184, 'Ascension St. Vincent - Indianapolis', 'ascensionst-vincent-in', '1', '2021-03-30 14:55:00', '2021-03-30 14:55:16', NULL, 0, 0, 0),
(185, 'Franciscan Health Indianapolis', 'franciscanhealthind2', '1', '2021-03-30 16:13:00', '2021-03-30 16:13:47', NULL, 0, 0, 0),
(186, 'Walgreens Local Specialty', 'walgreenslocalspecialty', '1', '2021-04-06 19:21:00', '2021-04-06 19:21:43', NULL, 0, 0, 0),
(187, 'Mercy Fairfield', 'mercyfairfield', '1', '2021-04-06 19:21:00', '2021-04-06 19:22:15', NULL, 0, 0, 0),
(188, 'Lancaster General Hospital Pharmacy', 'lancastergeneralhospharm', '1', '2021-04-09 14:00:00', '2021-04-09 14:00:53', NULL, 0, 0, 0),
(189, 'UF Shands', 'ufshands', '1', '2021-04-09 14:01:00', '2021-04-09 14:01:21', NULL, 0, 0, 0),
(190, 'Baptist Family Pharmacy', 'baptistfamilypharmacy', '1', '2021-04-09 14:01:00', '2021-04-09 14:01:48', NULL, 0, 0, 0),
(191, 'Ochsner LSU Health', 'ochsnerlsuhealth', '1', '2021-04-09 14:02:00', '2021-04-09 14:02:18', NULL, 0, 0, 0),
(192, 'Unity Point Health Care', 'unitypointhealthcare', '1', '2021-04-09 14:02:00', '2021-04-09 14:02:41', NULL, 0, 0, 0),
(193, 'Mercy Health System St Louis', 'mercyhealthsystemst-louis', '1', '2021-04-13 14:30:00', '2021-04-13 14:30:58', NULL, 0, 0, 0),
(194, 'Baptist ER', 'baptist-er', '1', '2021-04-13 14:31:00', '2021-04-13 14:31:20', NULL, 0, 0, 0),
(195, 'University of Mississippi Medical Center', 'ummc-booze', '1', '2021-04-15 21:35:00', '2021-04-15 21:36:04', NULL, 0, 0, 0),
(196, 'University of Mississippi Medical Center', 'ummc-bullock', '1', '2021-04-15 21:36:00', '2021-04-15 21:36:28', NULL, 0, 0, 0),
(197, 'University of Mississippi Medical Center', 'ummc-qualls', '1', '2021-04-15 21:36:00', '2021-04-15 21:36:54', NULL, 0, 0, 0),
(198, 'University of Mississippi Medical Center', 'ummc-griffith', '1', '2021-04-15 21:37:00', '2021-04-15 21:37:41', NULL, 0, 0, 0),
(199, 'University of Mississippi Medical Center', 'ummc-davis', '1', '2021-04-15 21:38:00', '2021-04-15 21:38:28', NULL, 0, 0, 0),
(200, 'University of Mississippi Medical Center', 'ummc-macklin', '1', '2021-04-15 21:39:00', '2021-04-15 21:39:18', NULL, 0, 0, 0),
(201, 'University of Mississippi Medical Center', 'ummc-hubbard', '1', '2021-04-15 21:39:00', '2021-04-15 21:39:48', NULL, 0, 0, 0),
(202, 'University of Mississippi Medical Center', 'ummc-jones', '1', '2021-04-15 21:40:00', '2021-04-15 21:40:17', NULL, 0, 0, 0),
(203, 'University of Mississippi Medical Center', 'ummc-brown', '1', '2021-04-15 21:40:00', '2021-04-15 21:40:38', NULL, 0, 0, 0),
(204, 'Family First Macomb', 'familyfirstmacomb', '1', '2021-04-15 21:40:00', '2021-04-15 21:41:07', NULL, 0, 0, 0),
(205, 'Reading Hospital', 'readinghospital3', '1', '2021-04-19 20:10:00', '2021-04-19 20:11:09', NULL, 0, 0, 0),
(206, 'Reading Hospital', 'readinghospital4', '1', '2021-04-19 20:11:00', '2021-04-19 20:11:33', NULL, 0, 0, 0),
(207, 'Reading Hospital', 'readinghospital5', '1', '2021-04-19 20:11:00', '2021-04-19 20:12:00', NULL, 0, 0, 0),
(208, 'Mayo Clinic FL', 'mayoclinicfl', '1', '2021-04-21 20:46:00', '2021-04-21 20:46:55', NULL, 0, 0, 0),
(209, 'Halifax Health', 'halifaxhealth', '1', '2021-04-21 20:47:00', '2021-04-21 20:47:20', NULL, 0, 0, 0),
(210, 'University of Louisville', 'universityoflouisville', '1', '2021-04-21 20:47:00', '2021-04-21 20:47:44', NULL, 0, 0, 0),
(211, 'HCA Centennial', 'hcacentennial', '1', '2021-04-21 20:47:00', '2021-04-21 20:48:08', NULL, 0, 0, 0),
(212, 'Henderson County Medical Group', 'hendersoncountymedicalgroup', '1', '2021-04-21 20:48:00', '2021-04-21 20:48:39', NULL, 0, 0, 0),
(213, 'Lehigh Valley Muhlenberg', 'lehighvalleymuhlenberg', '1', '2021-04-22 14:14:00', '2021-04-22 14:14:43', NULL, 0, 0, 0),
(214, 'University Medical Center El Paso', 'universitymedicalcenter-el-paso', '1', '2021-04-27 15:02:00', '2021-04-27 15:03:07', NULL, 0, 0, 0),
(215, 'St. Joseph Hospital Continuing Care Hospital', 'stjosephhospitalcch', '1', '2021-04-27 15:03:00', '2021-04-27 15:03:46', NULL, 0, 0, 0),
(216, 'Norton Healthcare', 'nortonhealthcare', '1', '2021-04-30 12:51:00', '2021-04-30 12:52:04', NULL, 0, 0, 0),
(217, 'McLaren Macomb Kipp', 'mclarenmacomb-kipp', '1', '2021-04-30 12:52:00', '2021-04-30 12:52:37', NULL, 0, 0, 0),
(218, 'McLaren Macomb Tokarski', 'mclarenmacomb-tokarski', '1', '2021-04-30 12:56:00', '2021-04-30 12:56:13', NULL, 0, 0, 0),
(219, 'McLaren Macomb Hermann', 'mclarenmacomb-hermann', '1', '2021-04-30 12:56:00', '2021-04-30 12:56:41', NULL, 0, 0, 0),
(220, 'Loma Linda Medical Center', 'lomalindamedicalcenter', '1', '2021-04-30 12:56:00', '2021-04-30 12:57:09', NULL, 0, 0, 0),
(221, 'University of Arkansas Gastroenterology', 'universityofarkansasgastro', '1', '2021-05-04 15:28:00', '2021-05-04 15:28:23', NULL, 0, 0, 0),
(222, 'Baptist Health Floyd', 'baptisthealthfloyd2', '1', '2021-05-04 15:56:00', '2021-05-04 15:57:06', NULL, 0, 0, 0),
(223, 'Georgetown Community Hospital', 'georgetowncommunityhospital', '1', '2021-05-07 20:53:00', '2021-05-07 20:53:23', NULL, 0, 0, 0),
(224, 'Self Regional Health Care', 'selfregionalhealthcare', '1', '2021-05-07 20:53:00', '2021-05-07 20:53:48', NULL, 0, 0, 0),
(225, 'HUP', 'hup', '1', '2021-05-07 20:53:00', '2021-05-07 20:54:10', NULL, 0, 0, 0),
(226, 'Christus Santa Rosa Hospital - Westover Hills', 'christussantarosahospital-westoverhills', '1', '2021-05-12 15:37:00', '2021-05-12 15:38:10', NULL, 0, 0, 0),
(227, 'Tampa General', 'tampageneral', '1', '2021-05-13 18:45:00', '2021-05-13 18:47:33', NULL, 0, 0, 0),
(228, 'UAB', 'uabmc', '1', '2021-05-19 13:05:00', '2021-05-19 13:06:11', NULL, 0, 0, 0),
(229, 'Fall Hill Gastro', 'fallhillgastro', '1', '2021-05-21 16:08:00', '2021-05-21 16:10:41', NULL, 0, 0, 0),
(230, 'FHGA', 'fhga', '1', '2021-05-21 16:11:00', '2021-05-21 16:12:15', NULL, 0, 0, 0),
(231, 'Baton Rouge General Taylor', 'batonrougegeneral-taylor', '1', '2021-05-27 18:25:00', '2021-05-27 18:26:07', NULL, 0, 0, 0),
(232, 'Baton Rouge General Guerin', 'batonrougegeneral-guerin', '1', '2021-05-27 18:27:00', '2021-05-27 18:27:29', NULL, 0, 0, 0),
(233, 'Plateau Clinic', 'plateauclinic', '1', '2021-05-27 18:28:00', '2021-05-27 18:29:04', NULL, 0, 0, 0),
(234, 'Beaumont Farmington Hills', 'beaumontfarmingtonhills', '1', '2021-06-02 20:14:00', '2021-06-02 20:15:02', NULL, 0, 0, 0),
(235, 'Hospital of the University of Pennsylvania', 'hospitaluniversitypennsylvania', '1', '2021-06-02 20:15:00', '2021-06-02 20:16:04', NULL, 0, 0, 0),
(236, 'Charlottesville', 'charlottesville', '1', '2021-06-03 20:03:00', '2021-06-03 20:04:05', NULL, 0, 0, 0),
(237, 'University of Kansas Medical Center', 'universitykansasmedicalcenter', '1', '2021-06-03 20:04:00', '2021-06-03 20:04:59', NULL, 0, 0, 0),
(238, 'Chandler Regional', 'chandlerregional', '1', '2021-06-07 19:12:00', '2021-06-07 19:13:08', NULL, 0, 0, 0),
(239, 'Valleywise Health', 'valleywisehealth', '1', '2021-06-07 19:13:00', '2021-06-07 19:14:20', NULL, 0, 0, 0),
(240, 'Bergen pharmacy', 'bergenpharmacy', '1', '2021-06-08 13:19:00', '2021-06-08 13:21:01', NULL, 0, 0, 0),
(241, 'Ascension Southfield Providence', 'ascensionsouthfieldprovidence-kelly', '1', '2021-06-14 13:48:00', '2021-06-14 13:48:23', NULL, 0, 0, 0),
(242, 'Ascension Providence Southfield', 'ascensionprovidencesouthfield-karaffa', '1', '2021-06-14 13:48:00', '2021-06-14 13:49:17', NULL, 0, 0, 0),
(243, 'Ascension Southfield Providence', 'ascensionsouthfieldprovidence-english', '1', '2021-06-14 13:50:00', '2021-06-14 13:50:22', NULL, 0, 0, 0),
(244, 'Charlottesville Gastroenterology Associates', 'charlottesvillegastroenterologyassociates', '1', '2021-06-14 13:51:00', '2021-06-14 13:52:25', NULL, 0, 0, 0),
(245, 'Southfield Providence', 'ascensionsouthfieldprovidence-jedrze', '1', '2021-06-14 13:52:00', '2021-06-14 13:53:27', NULL, 0, 0, 0),
(246, 'Ascension Providence Southfield', 'ascensionprovidencesouthfield-burns', '1', '2021-06-14 13:57:00', '2021-06-14 13:58:15', NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `clients_old`
--

CREATE TABLE `clients_old` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients_old`
--

INSERT INTO `clients_old` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Piyush', 'piyush', '1', '2020-05-27 10:33:06', '2020-05-27 10:33:06');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:100\"}}', 2),
(24, 4, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"},\"validation\":{\"rule\":\"required|unique:clients\"}}', 3),
(25, 4, 'status', 'select_dropdown', 'Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"},\"validation\":{\"rule\":\"required\"}}', 4),
(26, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(27, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(28, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(29, 5, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(30, 5, 'spanish_title', 'text', 'Spanish Title', 0, 1, 1, 1, 1, 1, '{}', 4),
(31, 5, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|unique:resources\"},\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 5),
(32, 5, 'menu_type', 'select_dropdown', 'Menu Type', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"1\":\"Provider\",\"2\":\"Caregiver and Paitients\",\"3\":\"Prescribing Information\",\"4\":\"Important Information\"},\"validation\":{\"rule\":\"required\"}}', 6),
(33, 5, 'category_type', 'select_dropdown', 'Category Type', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"1\":\"Resources\",\"2\":\"Fillable Forms\",\"3\":\"Videos\"},\"validation\":{\"rule\":\"required\"}}', 7),
(34, 5, 'pdf', 'file', 'Pdf', 0, 1, 1, 1, 1, 1, '{\"preserveFileUploadName\":\"yes\"}', 8),
(35, 5, 'spanish_pdf', 'file', 'Spanish Pdf', 0, 1, 1, 1, 1, 1, '{}', 9),
(36, 5, 'english_description', 'text_area', 'English Description', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 10),
(37, 5, 'spanish_description', 'text', 'Spanish Description', 0, 1, 1, 1, 1, 1, '{}', 11),
(38, 5, 'video_link', 'text', 'Video Link', 0, 1, 1, 1, 1, 1, '{}', 12),
(39, 5, 'cms_link', 'text', 'Cms Link', 0, 1, 1, 1, 1, 1, '{}', 13),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 14),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(42, 5, 'can_print', 'select_dropdown', 'Can Print', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"},\"validation\":{\"rule\":\"required\"}}', 2),
(43, 4, 'member_logo', 'image', 'Member Logo', 0, 1, 1, 1, 1, 1, '{}', 7),
(44, 4, 'member_logo_status', 'select_dropdown', 'Member Logo Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"Inactive\",\"1\":\"Active\"}}', 8),
(45, 4, 'sponser_status', 'select_dropdown', 'Sponser Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"Inactive\",\"1\":\"Active\"}}', 9),
(46, 4, 'hamburger_status', 'select_dropdown', 'Hamburger Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"Inactive\",\"1\":\"Active\"}}', 10),
(47, 5, 'no_print_title', 'select_dropdown', 'No Print Title', 0, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"Hide\",\"1\":\"Show\"}}', 16),
(48, 5, 'separate_pi', 'select_dropdown', 'Does this PDF includes separate PI', 0, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}', 17),
(50, 5, 'pdf_with_pi', 'file', 'Pdf With Pi', 0, 1, 1, 1, 1, 1, '{\"preserveFileUploadName\":\"yes\"}', 18);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-02-12 00:18:46', '2020-02-12 00:18:46'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-02-12 00:18:46', '2020-02-12 00:18:46'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2020-02-12 00:18:46', '2020-02-12 00:18:46'),
(4, 'clients', 'clients', 'Client', 'Clients', 'voyager-dot', 'App\\Client', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-12 01:06:52', '2020-08-31 17:09:42'),
(5, 'resources', 'resources', 'Resource', 'Resources', 'voyager-dot', 'App\\Resource', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-12 05:03:33', '2020-12-23 14:37:21');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-02-12 00:18:49', '2020-02-12 00:18:49');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-02-12 00:18:49', '2020-02-12 00:18:49', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2020-02-12 00:18:49', '2020-02-12 00:18:49', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-02-12 00:18:49', '2020-02-12 00:18:49', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-02-12 00:18:49', '2020-02-12 00:18:49', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2020-02-12 00:18:49', '2020-02-12 00:18:49', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2020-02-12 00:18:49', '2020-02-12 00:18:49', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2020-02-12 00:18:50', '2020-02-12 00:18:50', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2020-02-12 00:18:50', '2020-02-12 00:18:50', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2020-02-12 00:18:50', '2020-02-12 00:18:50', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2020-02-12 00:18:50', '2020-02-12 00:18:50', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2020-02-12 00:18:53', '2020-02-12 00:18:53', 'voyager.hooks', NULL),
(12, 1, 'Clients', '', '_self', 'voyager-dot', NULL, NULL, 15, '2020-02-12 01:06:52', '2020-02-12 01:06:52', 'voyager.clients.index', NULL),
(13, 1, 'Resources', '', '_self', 'voyager-dot', NULL, NULL, 16, '2020-02-12 05:03:34', '2020-02-12 05:03:34', 'voyager.resources.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(2, 'browse_bread', NULL, '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(3, 'browse_database', NULL, '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(4, 'browse_media', NULL, '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(5, 'browse_compass', NULL, '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(6, 'browse_menus', 'menus', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(7, 'read_menus', 'menus', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(8, 'edit_menus', 'menus', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(9, 'add_menus', 'menus', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(10, 'delete_menus', 'menus', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(11, 'browse_roles', 'roles', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(12, 'read_roles', 'roles', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(13, 'edit_roles', 'roles', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(14, 'add_roles', 'roles', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(15, 'delete_roles', 'roles', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(16, 'browse_users', 'users', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(17, 'read_users', 'users', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(18, 'edit_users', 'users', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(19, 'add_users', 'users', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(20, 'delete_users', 'users', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(21, 'browse_settings', 'settings', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(22, 'read_settings', 'settings', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(23, 'edit_settings', 'settings', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(24, 'add_settings', 'settings', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(25, 'delete_settings', 'settings', '2020-02-12 00:18:51', '2020-02-12 00:18:51'),
(26, 'browse_hooks', NULL, '2020-02-12 00:18:53', '2020-02-12 00:18:53'),
(27, 'browse_clients', 'clients', '2020-02-12 01:06:52', '2020-02-12 01:06:52'),
(28, 'read_clients', 'clients', '2020-02-12 01:06:52', '2020-02-12 01:06:52'),
(29, 'edit_clients', 'clients', '2020-02-12 01:06:52', '2020-02-12 01:06:52'),
(30, 'add_clients', 'clients', '2020-02-12 01:06:52', '2020-02-12 01:06:52'),
(31, 'delete_clients', 'clients', '2020-02-12 01:06:52', '2020-02-12 01:06:52'),
(32, 'browse_resources', 'resources', '2020-02-12 05:03:34', '2020-02-12 05:03:34'),
(33, 'read_resources', 'resources', '2020-02-12 05:03:34', '2020-02-12 05:03:34'),
(34, 'edit_resources', 'resources', '2020-02-12 05:03:34', '2020-02-12 05:03:34'),
(35, 'add_resources', 'resources', '2020-02-12 05:03:34', '2020-02-12 05:03:34'),
(36, 'delete_resources', 'resources', '2020-02-12 05:03:34', '2020-02-12 05:03:34');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1);

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE `resources` (
  `id` int(10) UNSIGNED NOT NULL,
  `can_print` tinyint(3) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spanish_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pdf` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spanish_pdf` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `english_description` longtext COLLATE utf8mb4_unicode_ci,
  `spanish_description` longtext COLLATE utf8mb4_unicode_ci,
  `video_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cms_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `no_print_title` tinyint(3) UNSIGNED DEFAULT NULL,
  `separate_pi` tinyint(3) UNSIGNED DEFAULT NULL,
  `pdf_with_pi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `resources`
--

INSERT INTO `resources` (`id`, `can_print`, `title`, `spanish_title`, `slug`, `menu_type`, `category_type`, `pdf`, `spanish_pdf`, `english_description`, `spanish_description`, `video_link`, `cms_link`, `created_at`, `updated_at`, `no_print_title`, `separate_pi`, `pdf_with_pi`) VALUES
(1, 1, 'Considerations for PAs', 'Considerations for PAs', 'considerations-for-pas', '1', '1', '[{\"download_link\":\"resources\\/February2021\\/1_PA_Considerations_Brochure1.pdf\",\"original_name\":\"1_PA_Considerations_Brochure.pdf\"}]', '[]', 'Provides guidance for submitting a prior authorization for XIFAXAN when needed', NULL, NULL, NULL, '2020-02-29 02:23:00', '2021-02-04 18:37:33', 0, 1, '[{\"download_link\":\"resources\\/February2021\\/1_PA_Considerations_Brochure_PI1.pdf\",\"original_name\":\"1_PA_Considerations_Brochure_PI.pdf\"}]'),
(2, 1, 'Counseling Tips', 'Counseling Tips', 'counseling-tips', '1', '1', '[{\"download_link\":\"resources\\/July2020\\/2_Counseling_Tips.pdf\",\"original_name\":\"2_Counseling_Tips.pdf\"}]', '[]', 'Whether your patient is newly diagnosed or is just newly on XIFAXAN, give him/her the support needed to help reduce the risk of OHE recurrence.', NULL, NULL, NULL, '2020-02-29 02:29:00', '2020-07-21 03:42:01', 0, 1, '[{\"download_link\":\"resources\\/July2020\\/2_Counseling_Tips_PI.pdf\",\"original_name\":\"2_Counseling_Tips_PI.pdf\"}]'),
(3, 1, 'Low-Income Subsidy Guide', 'Low-Income Subsidy Guide', 'low-income-subsidy-guide', '1', '1', '[{\"download_link\":\"resources\\/July2020\\/3_LIS_Brochure.pdf\",\"original_name\":\"3_LIS_Brochure.pdf\"}]', '[]', 'Your eligible patients may be entitled to medications for no more than $8.95/prescription, which is a Medicare Part D benefit.', NULL, NULL, NULL, '2020-02-29 02:29:00', '2020-07-21 03:42:32', 0, 0, '[{\"download_link\":\"resources\\/July2020\\/3_LIS_Brochure1.pdf\",\"original_name\":\"3_LIS_Brochure.pdf\"}]'),
(4, 1, 'Hepatic Encephalopathy Hospital Discharge Tips', 'Hepatic Encephalopathy Hospital Discharge Tips', 'hepatic-encephalopathy-hospital-discharge-tips', '1', '1', '[{\"download_link\":\"resources\\/July2020\\/4_HE_Hospital_Discharge_Tips.pdf\",\"original_name\":\"4_HE_Hospital_Discharge_Tips.pdf\"}]', '[]', 'Offers information providers should share when a patient is discharged to them from the hospital', NULL, NULL, NULL, '2020-02-29 02:29:00', '2020-07-21 03:43:16', 0, 1, '[{\"download_link\":\"resources\\/July2020\\/4_HE_Hospital_Discharge_Tips_PI.pdf\",\"original_name\":\"4_HE_Hospital_Discharge_Tips_PI.pdf\"}]'),
(5, 0, 'Letter of Medical Necessity', 'Letter of Medical Necessity', 'letter-of-medical-necessity', '1', '2', '[{\"download_link\":\"resources\\/July2020\\/5_Letter_of_Medical_Necessity.pdf\",\"original_name\":\"5_Letter_of_Medical_Necessity.pdf\"}]', '[]', 'A standard form for a patient-specific letter of medical necessity to explain your clinical decision making in choosing a therapy', NULL, NULL, NULL, '2020-02-29 02:33:00', '2020-07-21 03:41:13', 0, 0, '[{\"download_link\":\"resources\\/July2020\\/5_Letter_of_Medical_Necessity1.pdf\",\"original_name\":\"5_Letter_of_Medical_Necessity.pdf\"}]'),
(6, 0, 'Tier Exception Request', 'Tier Exception Request', 'tier-exception-request', '1', '2', '[{\"download_link\":\"resources\\/July2020\\/6_Tier_Exception_Request_Form.pdf\",\"original_name\":\"6_Tier_Exception_Request_Form.pdf\"}]', '[]', 'Use to reduce the cost share of a medication. Follows submission of a PA or Utilization Management request.', NULL, NULL, NULL, '2020-02-29 02:34:00', '2020-07-21 03:40:06', 0, 0, '[{\"download_link\":\"resources\\/July2020\\/6_Tier_Exception_Request_Form1.pdf\",\"original_name\":\"6_Tier_Exception_Request_Form.pdf\"}]'),
(7, 0, 'CoverMyMeds<sup>®</sup> ePA*', 'CoverMyMeds® ePA', 'covermymeds-r-epa', '1', '2', '[]', '[]', 'Use your free login to submit ePA form.', NULL, NULL, 'https://cc-cms.online/cmsbuilder/page_preview/dist/27ba725eb7ed769293ab3394c7b2e098', '2020-02-29 02:35:00', '2020-06-02 11:36:23', NULL, NULL, NULL),
(8, 0, 'XIFAXAN Instant Savings Card', 'XIFAXAN Instant Savings Card', 'xifaxan-instant-savings-card', '2', '1', '[]', '[]', 'Provides most eligible, commercially insured patients help with their monthly co-pays for XIFAXAN', NULL, NULL, 'https://cc-cms.online/cmsbuilder/page_preview/dist/a90aca18d44f92449001de74465ff685', '2020-02-29 02:36:00', '2020-06-08 17:01:47', 1, NULL, NULL),
(9, 1, 'Reduce the Risk of OHE Recurrence With XIFAXAN', 'Reduce the Risk of OHE Recurrence With XIFAXAN', 'reduce-the-risk-of-ohe-recurrence-with-xifaxan', '2', '1', '[{\"download_link\":\"resources\\/July2020\\/9_Reduce_Risk_of_OHE_Recurrence.pdf\",\"original_name\":\"9_Reduce_Risk_of_OHE_Recurrence.pdf\"}]', '[]', 'Provides information about XIFAXAN insurance coverage and prescription savings', NULL, NULL, NULL, '2020-02-29 02:38:00', '2020-07-21 03:38:43', 0, 1, '[{\"download_link\":\"resources\\/July2020\\/9_Reduce_Risk_of_OHE_Recurrence_PI.pdf\",\"original_name\":\"9_Reduce_Risk_of_OHE_Recurrence_PI.pdf\"}]'),
(10, 1, 'Setting up Medical Alerts on Digital Devices', 'Setting up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', '2', '1', '[{\"download_link\":\"resources\\/July2020\\/10_Setting_up_Medical_Alerts.pdf\",\"original_name\":\"10_Setting_up_Medical_Alerts.pdf\"}]', '[]', 'Instructions for setting up alerts on an iPhone, Android, or Apple Watch', NULL, NULL, NULL, '2020-02-29 02:41:00', '2020-07-21 03:37:29', 0, 0, '[{\"download_link\":\"resources\\/July2020\\/10_Setting_up_Medical_Alerts1.pdf\",\"original_name\":\"10_Setting_up_Medical_Alerts.pdf\"}]'),
(11, 0, 'Managing Overt Hepatic Encephalopathy With XIFAXAN', 'Managing Overt Hepatic Encephalopathy With XIFAXAN', 'managing-overt-hepatic-encephalopathy-with-xifaxan', '2', '3', '[]', '[]', 'This video will help patients with OHE understand their medication and the importance of ongoing disease management to help reduce the risk of OHE recurrence.', NULL, 'https://youtu.be/MHQ1_GzCSvc', NULL, '2020-02-29 02:42:00', '2020-06-08 19:50:49', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-02-12 00:18:50', '2020-02-12 00:18:50'),
(2, 'user', 'Normal User', '2020-02-12 00:18:50', '2020-02-12 00:18:50');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Quick Support Access', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Quick Support Access Admin Panel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings/June2020/X7Yn0ciHDOneCBeevC6Z.jpg', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'QSA Admin', 'qsa@admin.com', 'users/April2020/8UPTrTzGx4Ht4VGjBMae.jpg', NULL, '$2y$10$qua0gBNcer4FDkcMavlzI.eSubBI.a2VqGKwdnEN0zC1ZFnxNw6Gy', '4a4piSmq6lDQjKDAqsMkeZSfBpwsb2O4zzGxGKvqiaSgwcqr2NOk199MFIBb', '{\"locale\":\"en\"}', '2020-02-12 00:39:18', '2020-12-29 15:28:42');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients_old`
--
ALTER TABLE `clients_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `resources`
--
ALTER TABLE `resources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `clients_old`
--
ALTER TABLE `clients_old`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `resources`
--
ALTER TABLE `resources`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
